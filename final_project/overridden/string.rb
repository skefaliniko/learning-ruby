class String
    def self.restore_object_for_data_from_string(input)
        params = input.split(", ")
        new_apartment = Apartment.new(:number => params[0], :floor => params[1], :room_count => params[2].chomp)
        if new_apartment.floor != "ground"
            new_apartment.with_sea_view = true
        else
            new_apartment.with_sea_view = false
        end
        new_apartment
    end
end