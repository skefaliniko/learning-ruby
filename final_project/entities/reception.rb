class Reception
    include WorkWithItems
    include SendViaEmail

    def initialize
        @items = Array.new
    end

    def prepare_data
        puts "Getting info about apartments....."
        MyIO::FileIO.load_from_file do |each_line|
            add_item String.restore_object_for_data_from_string(each_line)
        end
        unless @items
            @items = create_test_data
        end
    end

    def delete_from_database(item)
        remove_item(item)
    end

    def load_info
        @items
    end

    def save_info(*output_file)
        puts "Saving data to file......"
        MyIO::FileIO.save_to_file @items, *output_file
    end

    private

    def create_test_data
        arr = []
        arr << Apartment.new(number: "123", floor: "ground", room_count: "1", with_sea_view: false)
        arr << Apartment.new(number: "234", floor: "first", room_count: "2", with_sea_view: true)
        arr << Apartment.new(number: "345", floor: "second", room_count: "3", with_sea_view: true)
        arr
    end
end