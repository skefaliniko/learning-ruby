class Report
    # include FileIO    #read some common pieces for report
    # include SendViaEmail  #maybe here

    def initialize(data_array)

    end

    def generate_report

    end

    def send_mail
        time_before_sending = Time.now  #formatted time from start of the Unix century: January, 1 1970

        sending_thread = Thread.new do
            Pony.mail({:to => '*****@gmail.com',
                       :from => "Test application <*****@yandex.ru>",
                       :via => :smtp,
                       :via_options => {
                           :address              => 'smtp-devices.yandex.com',
                           :port                 => '25',
                           :smtp_settings          => true,
                           :enable_starttls_auto => true,
                           :user_name            => '*****@yandex.ru', #the same as in From
                           :password             => '*****',
                           :authentication       => :plain
                       },
                       :subject => "Test message for 12_threads_pony....",
                       :body => "Please finish this part today!!",
                       :attachments => {"example.pdf" => File.read("booked_apartment.pdf")}
                      })
        end
        while sending_thread.alive? do
            print "."
            sleep(1)
        end

        time_after_sending = Time.now

        how_much = time_after_sending - time_before_sending
        puts "The email was sending in #{how_much.to_i} sec"

    end
    #
    # today = Time.now.utc    #UTS time
    # puts "Today is #{today.strftime("%b, %-d %Y %H:%M:%S")}" #Feb 1, 2018 18:24:11
    # puts
    #
    # send_mail

end