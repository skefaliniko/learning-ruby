class Apartment
    attr_accessor :number, :floor, :room_count, :with_sea_view

    def initialize(options = {})
        @number = options[:number]
        @floor = options[:floor]
        @room_count = options[:room_count]
        @with_sea_view = options[:with_sea_view]
    end

    def with_sea_view?
        @with_sea_view
    end

    def see_info
        yield(number)
        yield(floor)
        yield(room_count)
    end

    def to_s
        "#{@number}, #{@floor}, #{@room_count}"
    end
end