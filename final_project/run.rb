require_relative 'app_init/application'

Application.new do |app|
    app.name = "MyApp"
    app.config_env
    app.receptionist do |person|
        person.name = ENV['ADMIN_MAIL'].gsub(/@.*/, "").capitalize
        person.email = ENV['ADMIN_MAIL']
    end
end

print Application.name
puts " by #{Application::Admin.name}, <#{Application::Admin.email}>"
puts

Menu.new.start_conversation