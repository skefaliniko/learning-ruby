class Menu
    attr_reader :start, :greetings, :offer_a_choice, :choice, :params, :finished

    def initialize
        @reception = Reception.new
        prepare_to_speak
    end

    def method_missing(method_name, *args, &closure)
        if method_name.to_s =~ /(.+)\?$/
            return self.send($1.to_sym)
        else
            super
        end
    end

    def respond_to_missing?(method_name, include_private = false)
        method_name.to_s.end_with?('?') || super
    end

    def prepare_to_speak
        @bot_dialogs = {}
        @patterns = {}

        @bot_dialogs[:greeting] = "Hello! I am Jean-Claude Junker, a receptionist of EU's hotel. Welcome."
        @bot_dialogs[:misanderstanding] = "I don't understand. Please repeat."
        @bot_dialogs[:we_have_smth] = "We have some empty apartments. Just a moment..."
        @bot_dialogs[:here_you_are] = "Here are apartments with your params."
        @bot_dialogs[:search_by] = "Maybe you want me to show apartments by floor and|or room count. Just type 1 for floor, 2 for room count, 3 for both..."
        @bot_dialogs[:thanks] = "Thank you."
        @bot_dialogs[:type_params] = "Please type parameters of an apartment you want."
        @bot_dialogs[:choose_apartment] = "What apartment do you want to book?"
        @bot_dialogs[:dismiss] = "Ok. Here you are. I'll contact the administrator for booking the apartment"

        @patterns[:greeting] = /[Hh](ello|i)/
        @patterns[:to_find] = /[1-3]/
        @patterns[:apartment_floor] = /(ground|first|second)/
        @patterns[:apartment_rooms] = /([0-9]{1})/
        @patterns[:apartment_number] = /([0-9]{3})/

        @start = false
        @greetings = false
        @offer_a_choice = false
        @choice = false
        @params = false
        @finished = false
    end

    def check_answer(answer_array, pattern)
        right_answer = 0
        answer_array.each { |answer| right_answer += 1 if answer =~ pattern }
        right_answer == answer_array.length ? result = true : result =  false
        result
    end

    def start_conversation
        loop do
            unless start?
                @start = true
                puts @bot_dialogs[:greeting]
                answer = gets.chomp.split(", ") #return an array

                result = check_answer(answer, @patterns[:greeting])
                unless result
                    puts @bot_dialogs[:misanderstanding]
                    @start = false
                end
                next
            end

            unless greetings?
                @greetings = true
                puts @bot_dialogs[:we_have_smth]
                @reception.prepare_data
                @reception.load_info.each do |apartment|
                    apartment.see_info do |detail|
                        print "#{detail}\t"
                    end
                    puts "------"
                end
                puts @bot_dialogs[:here_you_are]
                next
            end

            unless choice?
                @choice = true
                puts @bot_dialogs[:search_by]
                answer = gets.chomp

                result = check_answer([answer], @patterns[:to_find])
                unless result
                    puts @bot_dialogs[:misanderstanding]
                    @choice = false
                    next
                end

                @user_wants_apartment = answer
                puts @bot_dialogs[:thanks]
                next
            end

            unless params?
                @params = true
                puts @bot_dialogs[:type_params]
                answer = gets.chomp.split(", ") #return an array
                #there is no check of answer because of variant where two params uses((
                #pls forgive me((
                case @user_wants_apartment.to_i
                    when 1
                        @apartments_user_wants = @reception.find_by_floor(answer[0])
                    when 2
                        @apartments_user_wants = @reception.find_by_room_count(answer[0])
                    when 3
                        @apartments_user_wants = @reception.find_by_floor_and_room_count(*answer)
                    else
                        puts "Nothing, sir(("
                end

                puts @bot_dialogs[:here_you_are]
                @apartments_user_wants.map {|apartment| puts apartment}
                next
            end

            unless offer_a_choice?
                @offer_a_choice = true
                puts @bot_dialogs[:choose_apartment]
                answer = gets.chomp

                result = check_answer([answer], @patterns[:to_find])
                unless result
                    puts @bot_dialogs[:misanderstanding]
                    @offer_a_choice = false
                    next
                end

                @booked_apartment = @apartments_user_wants.collect { |apartment| apartment if apartment.number == answer }
                @booked_apartment = @booked_apartment.compact[0]
                puts "Your choice:"
                puts "---------------------------------------------------"
                print @booked_apartment
                print " ..and is it with sea view.."
                if @booked_apartment.with_sea_view?
                    puts "Yes, great"
                else
                    puts "No, sorry...."
                end
                puts "---------------------------------------------------"
                next
            end

            unless finished?
                @finished = true
                puts @bot_dialogs[:dismiss]

                save_info_to_pdf @booked_apartment

                @reception.send_info_to_admin
                @reception.delete_from_database(@booked_apartment)
                @reception.save_info("saved.txt")
                break
            end
        end
    end

    def save_info_to_pdf(chosen_apartment)
        Prawn::Document.generate("booked_apartment.pdf") do
            text "This apartment has just been booked"
            text "The apartment number #{chosen_apartment.number} at #{chosen_apartment.floor} floor with #{chosen_apartment.room_count}"
        end
    end
end