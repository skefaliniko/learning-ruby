class Application
    class << self
        attr_accessor :name

        def new
            unless @instance
                yield(self)
                puts "Loading files.."
                require 'pony'
                require 'prawn'
                require_relative '../overridden/string'
                require_relative '../exceptions/not_permitted_for_writing_file'
                require_relative '../exceptions/no_such_file_error'
                require_relative '../modules/work_with_items'
                require_relative '../modules/my_io'
                require_relative '../modules/send_via_email'
                require_relative '../entities/reception'
                require_relative '../entities/apartment'
                require_relative '../app_init/menu'
            end
            @instance ||= self
            @instance.freeze
        end

        def config_env
            EnVarSetter.set_env
        end

        def receptionist(&closure)
            @receptionist ||= Application::Admin.new(&closure)
        end
    end

    class Admin
        class << self
            attr_accessor :name, :email

            def new
                unless @instance
                    yield(self)
                end
                @instance ||= self
                @instance.freeze
            end
        end
    end

    class EnVarSetter
        def self.set_env
            taken_keys = {}
            load_from_file do |each_line|
                keys_arr = each_line.chomp.split(" = ")
                taken_keys[keys_arr[0]] = keys_arr[1]
            end
            taken_keys.each_pair do |key, value|
                ENV[key.to_s] = value.to_s
            end
            puts "#{ENV['ADMIN_MAIL']}, #{ENV['RECEPTIONIST_MAIL']}" ###, #{ENV['RECEPTIONIST_PASSWORD']}"
        end

        def self.check_envar(key)
            puts "There is no info about #{key}" if ENV.has_key?(key.to_s) || ENV[key.to_s].nil?
        end

        def self.load_from_file(&closure)
            filename = "#{ENV['HOME']}/.secret_keys/admin/admin_keys.txt"
            regime = 'r'
            begin
                File.open(filename, regime) do |file|
                    file.each_line do |line|
                        closure.call(line)
                    end
                end
            rescue NoSuchFileError
                puts "No such file #{filename}"
            end
        end
    end
end