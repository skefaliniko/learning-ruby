module WorkWithItems
    def get_by_param(items_array, params)
        result = []
        key = params[0]
        value = params[1]
        items_array.each do |apartment|
            result << apartment if apartment.send(key.to_sym) == value
        end
        result
    end

    def method_missing(method_name, *args, &closure)
        if method_name.to_s =~ /^find_by_(.+)$/
            return run_searching(@items, $1, *args)
        else
            super
        end
    end

    # when overriding method_missing, respond_to_missing? must be overridden to
    def respond_to_missing?(method_name, include_private = false)
        method_name.to_s.start_with?('find_by_') || super
    end

    private
    def run_searching(searching_array, keys, *args)
        result = searching_array
        keys = keys.split('_and_')
        attrs_with_args = [keys, args].transpose
        attrs_with_args.each do |attr_arr|
            result = get_by_param(result, attr_arr)
        end
        result
    end

    def add_item(new_item)
        @items.push(new_item)
    end

    def remove_item(choice)
        unless choice
            puts "Nothing to choose"
        else
            @items.delete_at(@items.index(choice))
        end
    end
end