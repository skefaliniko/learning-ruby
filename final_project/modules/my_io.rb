module MyIO
    class FileIO
        def self.load_from_file(filename = 'input.txt', regime = 'r', &closure)
            begin
                File.open(filename, regime) do |file|
                    file.each_line do |line|
                        closure.call(line)
                    end
                end
            rescue NoSuchFileError
                puts "No such file #{filename}, the array may be empty"
            end
        end

        def self.save_to_file(data, filename = 'output.txt', regime = 'a')
            case regime
                when 'a', 'append', 'continue'
                    save_with(data, filename, 'a')
                when 'w', 'write', 'rewrite'
                    save_with(data, filename, 'w')
                else
                    save_with(data, filename, 'a')
            end
        end

        private
        def self.save_with(data, filename, regime)
            begin
                File.open(filename, regime) do |file|
                    data.each do |saving_object|
                        file.puts "#{saving_object}"    #conversion to string
                    end
                end
            rescue NotPermittedForWritingFile
                puts "Cannot save to file #{filename}"
            end
        end
    end
end