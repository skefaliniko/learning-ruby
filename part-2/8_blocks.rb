social_networks = ["twitter", "facebook", "instagram", "vkontakte", "odnoklassniki"]

social_networks.each { |socnet| puts socnet.upcase }

social_networks.each do |social|
	p "#{social} -> #{social.capitalize} -> #{social.upcase} ->  #{social.capitalize.downcase}"
	puts "-----"
end
puts
social_networks.each(&:upcase!)
puts social_networks
puts
puts "+++++++++++++++++++++++++++++++++++++++++++++++++++"
#------------------------------------------------

class Bookshelf
	attr_reader :books
	def initialize
		@books = Array.new
	end
	
	def set_book(book)
		@books.push book
	end

	def take_book
		@books.pop
	end

	def check_price
		@books.each { |book| puts "Sale! 20% discount for #{book.title} by #{book.author}" if book.price > 700 }
	end
	
	def see_all_books
		@books.each do |book|
			puts "Title: #{book.title}"
			puts "Author: #{book.author}"
			puts "Price: #{book.price}"
		end
	end

	#-----
	
	def take_book_with_price_upper(price)
		@books.delete_if { |item| item.price > price }	#	|| item.price.nil? 
	end
end

class Book
	attr_reader :title, :author, :price, :publisher
	def initialize(item)
		@title = item[:title]
		@author = item[:author]
		@price = item[:price]
	end
	
	#-----

	def see_details
		#NO	#[title, author, price, publisher]
		#NO	#title.to_s + ", " + author.to_s + ", " + price.to_s + ", " + publisher.to_s
		yield(title)	#FIRST CALL OF ANONIMOUS FUNCTION
		yield(author)	#, THEN SECOND CALL OF THE FUNCTION
		yield(price)	#, THEN THIRD CALL OF THE FUNCTION, STEP BY STEP AS ONE
		puts
	end
end


bookshelf = Bookshelf.new
bookshelf.set_book Book.new({:title => "Harry Potter and the Goblet of Fire", :author => "J.K.Rowling", :price => 789})
bookshelf.set_book Book.new({:title => "Lord of the Rings", :author => "R.K.K.Tolkien", :price => 956})
bookshelf.set_book Book.new({ :title => "Alice in the Wonderland", :author => "C.Lewise", :price => 467})
bookshelf.set_book Book.new({ :title => "Cursed child", :author => "J.K.Rowling", :price => 586})
bookshelf.set_book Book.new({ :title => "War and peace", :author => "L.Tolstoy", :price => 1409})

=begin
puts bookshelf.books
puts
puts bookshelf.take_book
puts
puts bookshelf.books
puts
=end
bookshelf.see_all_books
bookshelf.check_price

bookshelf.take_book
puts
bookshelf.check_price
puts
puts "===================================="
#-------------------------------------------------

=begin
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
bloks {} are like anonimous functions which have |some_iterator| as argument
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
=end

bookshelf.set_book Book.new({ :title => "War and peace", :author => "L.Tolstoy", :price => 1409})	# for 'nil' value do not set the key at all: ---:-p-r-i-c-e--=-> ...
bookshelf.books.each do |book|
	book.see_details { |attribute| puts attribute }
end

#---------------------------------------------------------------

bookshelf.set_book Book.new({ :title => "War or peace", :author => "T.Leo", :price => 735267465 })
puts
puts bookshelf.books.each do |book|
        book.see_details { |attribute| puts attribute }
end
puts

deleted = bookshelf.take_book_with_price_upper 500
deleted.each do |item|
	puts item.see_details { |attribute| puts attribute }
end

