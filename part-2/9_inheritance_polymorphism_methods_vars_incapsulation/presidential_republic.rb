require_relative 'unitary_republic'

class PresidentialRepublic < UnitaryRepublic
    attr_reader :president
    
    @@class_var = 40

    def initialize(options = {})
        @president = options[:president]
        puts "With class var @@class_var=40 #{@@class_var * 100}"
        super
    end
    
    #override
    def thanksgivings
        puts "#{@president}, you are the best president in the world!"
        
        europa_info
    end
    
    #override
    def show_info
        yield(president)
        super
        
        self.class.show_smth
        puts self.president
    end
    
private
    def europa_info
        puts "Europa is Europe)"
    end
end