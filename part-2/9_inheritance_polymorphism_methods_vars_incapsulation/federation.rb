require_relative 'republic'

class Federation < Republic
    def initialize(options = {})
        @head = options[:head]
        @object_counts = options[:object_count]
        
        super
    end
    
    def glory_for_country
        puts "We are #{@object_counts} republics leading by #{@head}. Soyuz nerushymykh respublic svobodnykh........"
    end
end