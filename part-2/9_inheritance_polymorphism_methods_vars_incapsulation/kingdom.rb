require_relative 'monarchy'

class Kingdom < Monarchy
    def initialize(options = {})
        @monarch = options[:monarch]
        @monarch_type = options[:monarch_type]
        super
    end
    
    def honor_to_monarch
        puts "Long live the #{@monarch_type} #{@monarch}!"
    end

    #override
    def collect_tax
        puts "Should I order or better ask a parliament?"
    end
end