class VirtualCountry
    attr_reader :name, :continent, :language, :capital

    def initialize(options = {})
        @name = options[:name]
        @capital = options[:capital]
        @continent = options[:continent]
        @language = options[:language]
    end
    
    def show_info
        # puts "#{@head} represents #{@name}, located in #{@continent}, with major language #{@language}"
        yield(name)
        yield(capital)
        yield(continent)
        yield(language)
    end
    
    def make_war_with(country)
        @enemy = country
        puts "There's a war between #{@name} and #{@enemy}"
    end
    
    def enemy?
        @enemy
    end
    
    def stop_war_with(country)
        @enemy = ''
        puts "Peace with #{@name} and #{country}!!"
    end
    
    def collect_tax
        puts "Virtual country can't collect the tax!"
    end
    
end
