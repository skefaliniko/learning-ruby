require_relative 'references'

virtual_country = VirtualCountry.new({ :name => "Limpopo", :capital => "None", :continent => "None", :language => "Russian" })

monarchy = Monarchy.new({ :name => "Lichtenstein", :capital => "Lichtenstein", :continent => "Europe", :language => "German" })
kingdom = Kingdom.new({ :name => "United Kingdom", :capital => "London", :continent => "Europe", :language => "English", :monarch => "Elisabeth II", :monarch_type => "Queen" })
empire = Empire.new({ :name => "Japan", :capital => "Tokyo", :continent => "Asia", :language => "Japanese", :emperor => "Akihito" })

republic = Republic.new({ :name => "First Galaxy Republic", :capital => "Corusanth", :continent => "The Galaxy", :language => "Common galaxian" })
federation = Federation.new({ :name => "USA", :capital => "Washington, DC.", :continent => "North America", :language => "English", :head => "Donald Trump", :object_counts => 50 })
unitary_republic = UnitaryRepublic.new({ :name => "Some", :capital => "none", :continent => "Europe", :language => "none" })
presidential_republic = PresidentialRepublic.new({ :name => "France", :capital => "Paris", :continent => "Europe", :language => "French", :president => "Emmanuel Macron" })
parliament_republic = ParliamentRepublic.new({ :name => "Greece", :capital => "Athens", :continent => "Europe", :language => "Greek", :prime_minister => "Alexis Tsipras" })

virtual_country.show_info { |attr| print "#{attr} " }
virtual_country.collect_tax
virtual_country.make_war_with "Zanzibar"
virtual_country.enemy?
virtual_country.stop_war_with "Zanzibar"
virtual_country.enemy?

puts "------------"

monarchy.show_info { |attr| print "#{attr} " }
monarchy.collect_tax
monarchy.make_war_with "Moscow tsardom"
monarchy.enemy?
monarchy.stop_war_with "Moscow tsardom"
monarchy.enemy?
monarchy.show_power

puts "------------"

kingdom.honor_to_monarch
kingdom.show_info { |attr| print "#{attr} " }
kingdom.show_power
kingdom.collect_tax
kingdom.make_war_with "Iraq"
kingdom.enemy?
kingdom.stop_war_with "Iraq"
kingdom.enemy?

puts "-------------"

empire.collect_tax
empire.show_info { |attr| print "#{attr} " }
empire.honor_to_emperor
empire.show_power
empire.make_war_with "Russia"
empire.enemy?
empire.stop_war_with "Russia"
empire.enemy?

puts "--------------"

republic.collect_tax
republic.enemy?
republic.stop_war_with "Smth..."
# republic.show_info                    # Error! because we didn't add yield for new properties into show_method

puts "--------------"

parliament_republic.thanksgivings

puts "--------------"

federation.glory_for_country

puts "--------------"

unitary_republic.thanksgivings

puts "--------------"

presidential_republic.thanksgivings

puts "--------------"

monarchy.collect_tax

puts "------"

presidential_republic.show_info { |attr| print "#{attr} " } #only ! property beacause there's no properties in parent? although there is super
puts

puts "Does the object 'presidential_republic' contain any method from virtual-state class?"
puts presidential_republic.respond_to?(:enemy?)
puts "for .enemy?"
puts presidential_republic.respond_to?(:prime_minister)
puts "for prime_minister"

puts "======================"

Republic.show_smth
Republic.some_method_dont_know_how_to_call

presidential_republic.show_info { |attr| print "#{attr} " }
# presidential_republic.europa_info -> private
presidential_republic.thanksgivings