require_relative 'unitary_republic'

class ParliamentRepublic < UnitaryRepublic
    def initialize(options = {})
        @prime_minister = options[:prime_minister]
        
        super
    end
    
    #override
    def thanksgivings
        puts "If you have a parliament with #{@prime_minister}, you have freedom"
    end
end