require_relative 'monarchy'

class Empire < Monarchy
    def initialize(options = {})
        @emperor = options[:emperor]
        super
    end
    
    def honor_to_emperor
        puts "Ave, #{@emperor}!"
    end

    #override
    def collect_tax
        puts "Gimme your money, or I put you into jail!"
    end
end