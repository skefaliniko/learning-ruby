require_relative 'virtual_country'

class Monarchy < VirtualCountry     #add methods AND PROPERTIES from parent class
    def initialize(options = {})
        #calls constructor of parent class, expression like super(options) instead super is optional
        super
    end
    
    def show_power
        puts "I am the head of #{name}! Kneels!"
    end
end