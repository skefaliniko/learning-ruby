class Book
    include Action::Destroy
    include Action::Heal
    
    attr_reader :title, :author, :price, :publisher
    
    def initialize(item)
        @title = item[:title]
        @author = item[:author]
        @price = item[:price]
    end
    
    def see_details
        yield(title)
        yield(author)
        yield(price)
    end
    
    #when we put items to command line with puts or print methods,
    #Ruby tries to convert type Item to string
    #if the method to_s is defined in class Item, then Item instances will automatically be converted without invoking #to_s
    def to_s
        "#{@title},#{@author},#{@price}"
    end
end
