class Bookshelf
    include ItemContainer
    include Action::Destroy
    include Action::Heal
    
    attr_accessor :items
    
    def initialize
        @items = Array.new  #array of books
    end
    
    def validate
        @items.each { |book| puts "Sale! 20% discount for #{book.title} by #{book.author}" if book.price > 700 }
    end
    
    def see_all_items
        @items.each do |book|
            # puts book.to_s
            puts book
        end
    end
    
    def take_book_with_price_upper(price)
        @items.delete_if { |item| item.price > price }
    end
    
    # def tmp(&block)
    #     block.call
    # end
end
