module Material
    def show_materials(target)
        puts "#{target.class.to_s} was made in China"
        puts
    end
    
    #there we get a reverence by 'item_class' to the class including module Material
    #(self is for module, not for the class instance)
    #It's a part of metaprogramming
    #For an example see gem Device by Jos`e Valim
    def self.included(item_class)
        puts "#{item_class} class included module #{self} before instantiating"
        puts "(we know because callback method 'include' was called)"
        puts "++++++ ++++++ +++++++ ++++++ +++++++ ++++++"
        
        #to add to the class all methods determined inside a class in this module,
        #we should use this expression:
        #   item_class.extend MODULE_NAME
        #----------------------------------------------
        #to add all instance's methods, use:
        #   item_class.class_eval do
        #      include MODULE_NAME
        #   end
    end
end