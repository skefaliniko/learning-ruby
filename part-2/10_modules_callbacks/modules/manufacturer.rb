module Manufacturer
    def show_manufacturer(item)
        case item.class.to_s
            when "Book"
                puts "#{item.class} #{item.title} by #{item.author} was made in Austria"
            when "Cup"
                puts "#{item.class} with #{item.coffee.name} was made in China"
            else
                puts "Unrecognized"
        end
    end
end

class ManufacturerIncluder
    include Manufacturer
end