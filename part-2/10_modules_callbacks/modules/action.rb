module Action
    module Destroy
        def crash_object(item)
            action = Action::CommonOperations.determine_action_to_destroy item
            puts "#{item.class.to_s} is ready to #{action}"
        end
    end
    
    module Heal
        def repair(item)
            action = Action::CommonOperations.determine_action_to_heal item
            puts "#{item.class.to_s} is ready to #{action}"
        end
    end

    class CommonOperations
        def self.determine_action_to_destroy(item)
            case item.class.to_s
                when "Book"
                    "tear up"
                when "Bookshelf", "Cup"
                    "break"
                when "CoffeeMaker"
                    "destroy"
            end
        end

        def self.determine_action_to_heal(item)
            case item.class.to_s
                when "Book"
                    "glue together"
                when "Bookshelf", "Cup"
                    "nail"
                when "CoffeeMaker"
                    "mend"
            end
        end
        
        def self.show_created(item)
            puts "#{item.class.to_s} was created"
        end
    end
end

class ActionIncluder
    include Action::Destroy
    include Action::Heal
end