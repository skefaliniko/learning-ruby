module ItemContainer
    def add_item(item)
        @items.push item
    end
    
    def take_item
        took = @items.pop
        item_name = ""
        
        if took.instance_of? Book
            item_name = took.title
        elsif took.instance_of? Cup
            item_name = took.coffee.name
        end
        
        self.show_what_picked item_name
        
        took
    end
    
    def show_what_picked(item_name)
        puts "You have taken #{item_name}"
    end
end