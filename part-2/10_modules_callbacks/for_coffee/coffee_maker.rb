class CoffeeMaker
    include ItemContainer
    include Material
    include Action::Destroy
    include Action::Heal

    attr_accessor :items

    def initialize
        @items = Array.new  #array of cups
    end
    
    def turn_on
        puts "Coffee machine is ready!"
        puts
    end
    
    def what_is_left
        @items.each do |cup|
            puts "We have: #{cup.coffee.name}"
        end
        puts
    end
end