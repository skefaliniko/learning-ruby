class Cup
    include Action::Destroy
    include Action::Heal
    
    attr_reader :coffee
    
    def initialize(coffee)
        @coffee = coffee
        argsh
    end
    
    private
    def argsh
        puts "I like #{@coffee.name} but it's very hot!"
        puts
    end
end