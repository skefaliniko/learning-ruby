class Coffee
    attr_reader :name
    
    def initialize(name)
        @name = name
        smell
    end
    
    private
    def smell
        puts "It smells very good!"
    end
end