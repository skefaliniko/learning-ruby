# require_relative 'interface_mb'

class Computer
    include Interface
    
    def initialize
        puts "Instance was created"
    end
    
    def start
        puts "Computer is started"
    end
    
    def shutdown
        puts "Computer is shut down"
    end
end