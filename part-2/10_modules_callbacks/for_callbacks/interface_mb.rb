module Interface
    def self.include(base_class)
        puts "#{base_class.class.to_s} was made in China"
    end
    
    module Some
        def some_method_of_some_module
            puts "some method of Some module was called. INCLUDE"
        end
    end
    
    module Another
        def another_method_of_another_module
            puts "another method of Another module was called. EXTEND"
        end
    end
    
    module RewriteShutdown
        def shutdown
            puts "Method from module RewriteShutdown overrided class method `shutdown`"
        end
    end
end