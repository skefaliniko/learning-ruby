require_relative 'modules/item_container' #require module before class where the module is used TO AVOID DUPLICATE require's
require_relative 'modules/material'
require_relative 'modules/action'

require_relative 'for_books/book'
require_relative 'for_books/bookshelf'

require_relative 'for_coffee/coffee'
require_relative 'for_coffee/cup'
require_relative 'for_coffee/coffee_maker'

require_relative 'modules/manufacturer'

require_relative 'for_callbacks/interface_mb'
require_relative 'for_callbacks/computer'

require_relative 'string'

bookshelf = Bookshelf.new

bookshelf.add_item Book.new({:title => "Harry Potter and the Goblet of Fire", :author => "J.K.Rowling", :price => 789})
bookshelf.add_item Book.new({:title => "Lord of the Rings", :author => "R.K.K.Tolkien", :price => 956})
bookshelf.add_item Book.new({:title => "Alice in the Wonderland", :author => "C.Lewise", :price => 467})
bookshelf.add_item Book.new({:title => "Cursed child", :author => "J.K.Rowling", :price => 586})
bookshelf.add_item Book.new({:title => "War and peace", :author => "L.Tolstoy", :price => 1409})

bookshelf.see_all_items
my_book = bookshelf.take_item
bookshelf.validate

puts
puts "You have a book, so it's time for reading with cup of coffee!"
puts "----------------------------------"

coffee_maker = CoffeeMaker.new
coffee_maker.turn_on

coffee_maker.add_item Cup.new Coffee.new("Cappiccino")
coffee_maker.add_item Cup.new Coffee.new("Latte")
coffee_maker.add_item Cup.new Coffee.new("Espresso")

coffee_maker.what_is_left
my_coffee_cup = coffee_maker.take_item
coffee_maker.what_is_left
coffee_maker.show_materials coffee_maker

manufacturer = ManufacturerIncluder.new
manufacturer.show_manufacturer(my_coffee_cup)
manufacturer.show_manufacturer(my_book)

puts
puts "-----------------------------------"
puts "Crash and heal"
puts

book_to_action = Book.new({:title => "Clean code"})
Action::CommonOperations.show_created book_to_action

bookshelf_to_action = Bookshelf.new
Action::CommonOperations.show_created bookshelf_to_action

cup_to_action = Cup.new Coffee.new("Russiano))")
Action::CommonOperations.show_created cup_to_action

coffee_maker_to_action = CoffeeMaker.new
Action::CommonOperations.show_created coffee_maker_to_action

puts
puts "Start crazy things)"
puts

array_of_dif_object = [book_to_action, bookshelf_to_action, cup_to_action, coffee_maker_to_action]
array_of_dif_object.each do |elem|
    action = ActionIncluder.new
    action.crash_object elem
    action.repair elem
    puts "--->> --->> --->> --->> --->>"
end

puts
puts "========================================="
puts "There we start to work with callback. Working with metaprogramming"

computer = Computer.new
computer.start
computer.shutdown

puts
print "Does #{computer.class.to_s} invokes included method? -> "
puts computer.respond_to? :some_method_of_some_module
print "Does #{computer.class.to_s} invokes extended method? -> "
puts computer.respond_to? :another_method_of_another_module

puts "... then ->"
puts "Extend there..."
computer.extend Interface::RewriteShutdown #rewrite method of instance?
computer.start
computer.shutdown

Computer.class_eval do
    include Interface::Some
end
print "And now...."
print "Does #{computer.class.to_s} invokes included method? -> "
puts computer.respond_to? :some_method_of_some_module
print "Does #{computer.class.to_s} invokes extended method? -> "
puts computer.respond_to? :another_method_of_another_module

puts
puts "Could not find methods from module Another ->"
Computer.extend Interface::Another      #add methods from Another module outside class description

puts
puts "Another try..........."
print "Does #{computer.class.to_s} invokes extended method? -> "
puts computer.class.respond_to? :another_method_of_another_module

puts "+++++++++++++++++++++++++++++++++++++++++++++"
puts "Summing up:"
puts "EXTENDS -> add new methods outside class description"
puts "INCLUDE -> add methods when module include"

#-------------------------------------------------------------
puts "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
puts "Conversion example"
book_to_convert = Book.new({:title => "Harry Potter and the Prisoner of Azkaban", :author => "J.K.Rowling", :price => 789})
book_in_string = book_to_convert.to_s
puts book_in_string
restored = String.restore_from_string book_in_string
puts restored.class
puts "^"
restored.see_details { |param| puts param }