
require 'pony'

def send_mail
	time_before_sending = Time.now	#formatted time from start of the Unix century: January, 1 1970
	
	sending_thread = Thread.new do
		Pony.mail({
  			:to => '***@gmail.com',
			:from => "Test application <*****@yandex.ru>",
	  		
			:via => :smtp, :via_options => {
	    			:address              => 'smtp-devices.yandex.com',
	    			:port                 => '25',
     				:smtp_settings		=> true,
   				:enable_starttls_auto => true, 
				:user_name            => '*****@yandex.ru', #the same as in From
	    			:password             => '***',
	    			:authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
	    			 # the HELO domain provided by the client to the server
			},
			:subject => "Test message for 12_threads_pony....",
			:body => "Please finish this part today!!",
			:attachments => {"example.pdf" => File.read("inplicit.pdf"), "hello.txt" => "hello!"}
		})
	end
	while(sending_thread.alive?) do
		print "."
		sleep(1)
	end

	time_after_sending = Time.now
	
	how_much = time_after_sending - time_before_sending
	puts "The email was sending in #{how_much.to_i} sec"

end

today = Time.now.utc	#UTS time
puts "Today is #{today.strftime("%b, %-d %Y %H:%M:%S")}" #Feb 1, 2018 18:24:11
puts

send_mail
=begin
#_______________________________________________________________________

puts "-------------------"

class Cloud
	attr_accessor :color, :place

	def initialize(color, place)
		@color = color
		@place = place
	end
	
	def what_am_i_doing
		puts "I am #{@color} #{self.class} flying #{@place}".chomp
	end
end

little_cloud = Cloud.new "Grey", "in heaven"
puts little_cloud.what_am_i_doing

little_cloud.color = "Dark grey"
puts little_cloud.what_am_i_doing

cloud_reference = little_cloud
puts cloud_reference.what_am_i_doing

clonned_cloud = little_cloud.clone	#dup + freeze
puts "The same? #{clonned_cloud == little_cloud}"

clonned_cloud.color = "Red"
puts clonned_cloud.what_am_i_doing
puts "Is frozen? #{clonned_cloud.frozen?}"

clonned_cloud.freeze
#clonned_cloud.color = "Green"
#puts clonned_cloud.what_am_i_doing
puts "Is frozen? #{clonned_cloud.frozen?}"

duplicated_cloud = little_cloud.dup.freeze
puts "The same? #{duplicated_cloud == little_cloud}"
=end
