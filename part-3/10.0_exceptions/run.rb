require_relative 'overrided/string'
require_relative 'entities/book'
require_relative 'modules/file_io'

array_of_book = FileIO.read_from_file 'books.txt'
array_of_book.each do |book|
    book.see_details { |param| print "#{param} " }
    puts
end
puts "-------------------"
FileIO.export_to_file 'saved_books.txt', array_of_book

little_book = Book.new({:title => "Anna Karenina", :author => "L.Tolstoy", :price => 700})
little_book.see_object_type
little_book.see_details { |param| print "#{param} " }
puts
little_book.see_something
puts
puts "Try to call method not defined (started with see_*):"
print "==>"
little_book.see_blah_blah
puts "Try to call another method not defined (but started with see_*):"
print "==>"
little_book.see_how_little_pony_dance
puts

puts "Try to call method not defined (but started with other keys):"
begin
    little_book.ping_fluffy_unicorn
rescue NoMethodError => e
    puts e.inspect
ensure
    little_book.see_blah_blah
end
