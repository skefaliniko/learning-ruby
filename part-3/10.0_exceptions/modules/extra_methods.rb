module InstanceMethods
    def method_missing(method_name) #this method is defined in parent class
        if method_name =~ /^see_/
            puts "Method shows something"
        else
            super   #catch exceptions in other variants
        end
    end
end

=begin
сlass ActiveRecord::Base
  def method_missing(meth, *args, &block)
    if meth.to_s =~ /^find_by_(.+)$/
      run_find_by_method($1, *args, &block)
    else
      super # Вы должны вызвать super если вы не собираетесь
      # использовать метод, иначе вы засорите поиск методов.
    end
  end

  def run_find_by_method(attrs, *args, &block)
  # Создает массив имен атрибутов
    attrs = attrs.split('_and_')

    # #transpose объединяет два массива вместе, пример:
    #   [[:a, :b, :c], [1, 2, 3]].transpose
    #   # => [[:a, 1], [:b, 2], [:c, 3]]
    attrs_with_args = [attrs, args].transpose

    # Hash[] получает массив и преобразует его в хэш:
    #   Hash[[[:a, 2], [:b, 4]]] # => { :a => 2, :b => 4 }
    conditions = Hash[attrs_with_args]

    # #where и #all это новые вкусности из AREL, которые позволяют найти
    # все записи соответствующие условию поиска
    where(conditions).all
  end
end
=end