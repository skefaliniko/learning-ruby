class FileIO
    def FileIO.read_from_file(input_file)
        @@items = []
        begin
            File.open(input_file, 'r') { |file|
                file.each_line do |line|
                    @@items << String.restore_from_string(line)
                end
            }
        rescue
            puts "Error reading from file #{input_file}. No such file?"
        end
        return @@items
    end

    def FileIO.export_to_file(output_filename, items = [])
        begin
            if items.nil?
                puts "Empty array of data"
            else
                items.each do |elem|
                    data = "#{elem.title},#{elem.author},#{elem.price}"
                    File.open(output_filename, 'a') do |file|     # 'a' for append!! 'w' for rewriting file
                        file.puts(data)
                    end
                end
            end
        rescue
            puts "Error writing to file #{output_filename}"
        end
    end
end