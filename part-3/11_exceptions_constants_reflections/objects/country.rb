require_relative '../interfaces/properties_info'

class Country
    include Properties

    attr_reader :name, :continent

    def initialize(options = {})
        #class Republic inherits class Country, where module Properties included
        #so all methods from the module will be available
        @name = options[:name]
        @continent = options[:continent]
    end
end