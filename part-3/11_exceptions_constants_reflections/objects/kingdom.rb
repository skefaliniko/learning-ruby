# require_relative 'country'

class Kingdom < Country
    attr_reader :name, :continent

    def initialize(options = {})
        #class Republic inherits class Country, where module Properties included
        #so all methods from the module will be avaiable
        @name = options[:name]
        @continent = options[:continent]
    end
end