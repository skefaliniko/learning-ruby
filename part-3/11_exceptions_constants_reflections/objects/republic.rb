# require_relative 'country'

class Republic < Country
    
    #class Republic inherits class Country, where module Properties included
    #so all methods from the module will be avaiable
    
    attr_reader :name, :continent
    
    def initialize(options = {})
        @name = options[:name]
        @continent = options[:continent]
    end
end

