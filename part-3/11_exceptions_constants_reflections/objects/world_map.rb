require_relative '../interfaces/file_operations'
require_relative '../exceptions/my_error'

class WorldMap
    include FileOperations
    
    attr_reader :items

    UNSUPPORTED_ITEMS = [:Country, :WorldMap, :Mount, :Fox]
    
    def initialize
        @items = Array.new
    end

    def add_item(item)
        #this will raise an error with program termination
        # raise "#{item.class} is not supported" if UNSUPPORTED_ITEMS.include? item.class.to_s.to_sym

        # another variant:
        # raise MyError::ItemNotSupported if UNSUPPORTED_ITEMS.include? item.class.to_s.to_sym
        begin
            @items << item
        # rescue MyError::ItemNotSupported
        raise MyError::ItemNotSupported if UNSUPPORTED_ITEMS.include? item.class.to_s.to_sym
        save_to_file('backup_data.txt', 'a') do |file|
            item.show_properties { |prop| file.print "#{prop} " }
            file.puts
            puts "Item #{item.class} was added or temporary saved"
        end
        # ensure
        #     puts "Something happened"
        end
    end

    def remove_item(country)
        begin
        index_to_delete = @items.index { |i| i.name == country }
        @items.delete_at index_to_delete
        rescue TypeError
            puts "Wrong type of index"
        end
    end

    def show_item_index(country)
        @items.index { |i| i.name == country }
    end
    
    def show_all
        self.items.each do
            |elem| elem.show_properties { |prop| print "#{prop}, " }
            puts
        end
        puts "-----"
    end
end