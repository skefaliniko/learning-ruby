require_relative 'exceptions/my_error'

require_relative 'objects/country'
require_relative 'objects/kingdom'
require_relative 'objects/republic'
require_relative 'objects/world_map'

require_relative 'objects/fox'
require_relative 'objects/mount'

country = Country.name
# country.show_properties   #there is no such method because class provides interface like abstract
                            # but it can be instantiated, so it is not truly abstract

republic = Republic.new({:name => "Zimbabwe", :continent => "Africa"})
republic.show_properties

puts "-------------------------------------"

roman_republic = Republic.new({:name => "Roman Republic", :continent => "Oikumena"})
roman_republic.show_properties { |param| puts param + " ---> ancient" }

puts "------------------------------------"

world_map = WorldMap.new

world_map.read_from_file do |file|
    file.each_line do |line|
        params = line.split(", ")
        case params[0]
            when "Republic"
                world_map.add_item Republic.new({:name => params[1], :continent => params[2]})
            when "Kingdom"
                world_map.add_item Kingdom.new({:name => params[1], :continent => params[2]})
            else
                puts "Something wrong!"
        end
    end
end

if world_map.nil?
    world_map.add_item Republic.new({:name => "France", :continent => "Europe"})
    world_map.add_item Republic.new({:name => "Austria", :continent => "Europe"})
    world_map.add_item Kingdom.new({:name => "Great Britain", :continent => "Europe"})
    world_map.add_item Kingdom.new({:name => "Saudi Arabia", :continent => "Asia"})
end

puts "#{world_map.show_item_index "Austria"} is the index of Austria in array"
puts "-------"

world_map.show_all
world_map.remove_item "France"
world_map.show_all

world_map.add_item Country.new({:name => "Lichtenstein", :continent => "Europe"})
world_map.add_item Fox.new
world_map.add_item Mount.new