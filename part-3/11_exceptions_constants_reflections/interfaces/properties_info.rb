module Properties
    def show_properties(&closure)
        closure ||= lambda { |property| puts property }
        closure.call name       #getter, not property
        closure.call continent  #getter, not property
    end
end