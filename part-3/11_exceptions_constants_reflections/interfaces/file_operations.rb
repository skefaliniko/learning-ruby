module FileOperations
    def read_from_file(file = 'input.txt', regime = 'r', &closure)
        begin
            File.open(file, regime, &closure)
        rescue Errno::ENOENT
            puts "No such file with countries' data"
        end
    end
    
    def save_to_file(file = 'output.txt', regime = 'w', &closure)
        begin
            File.open(file, regime, &closure)
        rescue Errno::ENOENT
            puts "Cannot save to file"
        end
    end
end