require_relative '../modules/extra_methods'
require_relative '../modules/item_container'

class SolarSystem
    include InstanceMethods
    include ItemContainer

    attr_reader :name

    def initialize
        @items = Array.new
        @name = "Solar System"
    end
end