class SpaceObject
    attr_reader :name, :form, :type

    def initialize(options = {})
        @name = options[:name]
        @form = options[:form]
        @type = options[:type]
    end

    def self.descendants
        ObjectSpace.each_object(Class).select { |klass| klass < self }
    end
end