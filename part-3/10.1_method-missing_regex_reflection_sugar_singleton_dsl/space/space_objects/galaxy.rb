#Example with three!!!! different variables, see run.rb

class Galaxy
    attr_accessor :authority

    @@authority = "Galactic Senate"

    def initialize(councelor)
        @authority = councelor
    end

    def rule
        puts "I, #{@authority}, say you: don't give up, #{@@authority} stays strong..."
    end

    class << self
        attr_accessor :authority

        def rule(emperor)
            @autority = emperor
            puts "Siths rules the Galaxy! #{@autority} takes over #{@@authority}"
        end
    end
end