class Star < SpaceObject
    attr_reader :temperature

    def initialize(options = {})
        @temperature = options[:temperature]
        super
    end
end