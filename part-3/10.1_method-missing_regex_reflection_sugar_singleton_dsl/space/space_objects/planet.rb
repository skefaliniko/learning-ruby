class Planet < SpaceObject
    attr_reader :surface

    def initialize(options = {})
        @surface = options[:surface]
        super   #super(option)
    end

    private
    def show_name
        self.name
    end
end