module InstanceMethods
    def method_missing(method_name, *arguments, &closure)
        run_method($1, *arguments) if method_name.to_s =~ /^find_by_(.+)$/
    rescue
        puts "There is no such attribute for an object"
    end

private

    def run_method(attributes, *arguments)
        attributes = attributes.split('_and_')          #example: find_by_name_and_type

        #transpose split two array together index to index:
        #   [[:a, :b, :c], [1, 2, 3]].transpose                 #=> [[:a, 1], [:b, 2], [:c, 3]]
        attrs_with_args = [attributes, arguments].transpose     #(example) #=> [[form, "Sphere"], [type, "Star"]]

        #Hash[] receives array and converts it to hash:  Hash[[[:a, 2], [:b, 4]]] #=> { :a => 2, :b => 4 }
        search_criteria = Hash[attrs_with_args]

        result = search_criteria.map { |key, value| search_by_key_and_value(key, value) }
        print result.flatten!.uniq    #convert multidimensional array to one-dimensional
        puts
        # closure.call result
    end

    def search_by_key_and_value(key, value)  #sym, str
        result = []
        @items.map { |space_object| result << space_object.name if space_object.send(key) == value }    #or with @items.find_all
        result
    end


    #=========================================================
    # idea: it should parse 2 or more params, finish later
    #=========================================================



    # def get_params(string)
    #     # matches = pattern.match string
    #     # result = []
    #     # result << matches[1] if matches.length <2    #??
    #     # result << split_params(matches[2], pattern)
    #
    #     result = []
    #
    #     pattern = /([a-z]{1,})((_and_[a-z]{1,}){1,})?/
    #
    #     # matches = string.scan(pattern)
    #     # matches.to_a.flatten!
    #
    #     matches = string.scan pattern
    #     matches = matches[0]
    #
    #     matches[1].gsub(/!(^_and_)/, "")
    #     # print matches[1].slice(/^_and_/)
    #
    #
    #     if matches.length <= 2
    #     # if string.size == 0
    #         print matches[0]
    #         puts "->"
    #     else
    #         result << get_params(matches[1].slice(/^_and_/))
    #         puts "==>"
    #     end
    #     result
    #
    #     # matches[2].nil? ? result << matches[1] : result << get_params(matches[2].gsub(/^_and_/))
    #
    # end
    #
    # puts get_params("name_and_type_and_type_and_type_and_type")
end