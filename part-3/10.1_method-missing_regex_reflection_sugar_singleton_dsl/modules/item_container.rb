module ItemContainer
    def add_item(item)
        @items.push item
    end

    def take_item
        @items.pop
    end
end