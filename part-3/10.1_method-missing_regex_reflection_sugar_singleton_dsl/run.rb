require_relative 'singletons/application'

Application.configure do |app|                #super))) it works

    #There is a DSL (domain-specific language) simple example
    #DSl is used to describe some model with understandable syntax
    #There are two kinds of DSl: extern and intern.
    #Extern are those which requires parser and not supported directly (CSS, XML, HAML, HTML, Linux config files, .....)
    #Intern are those which are described using keywords and expressions of a language DSLs are used in
    #   Such a code runs as usual, but it is not like the programming language much

    #   Example below: we have built our own DSL using common instruments of Ruby language

    app.name = "MyName"
    app.environment = :development # :production

    app.admin do |admin|
        admin.name = "John Smith"
        admin.email = "john_smith@gmail.com"
        admin.login = "jsmith"
        admin.nation = :reptilian
        admin.notify_on :wednesday #(or any other day)
    end
end

puts "Some info about the app"
puts Application.name
puts Application.environment
puts "Admin: #{Application::Admin.name}, nation: #{Application::Admin.nation}"


# Application.configure   #not working twice
# Application.configure   #not working three times

solar_system = SolarSystem.new

solar_system.add_item Planet.new({:name => "Mercury", :form => "square", :type => "planet", :surface => "hard"})

# solar_system.add_item Star.new({:name => "Sun", :form => "sphere", :type => "star", :temperature => "5778 K"})            #possible missing some properties

#There is only one param in initialize method of each class - options={}
#So when creating objects such brackets like {} we can remove:
solar_system.add_item Planet.new(:name => "Neptune", :form => "sphere", :type => "planet", :surface => "gaseous")
solar_system.add_item Planet.new(:name => "Venus", :form => "sphere", :type => "planet", :surface => "hard")
solar_system.add_item Planet.new(:name => "Saturn", :form => "sphere", :type => "planet", :surface => "gaseous")
solar_system.add_item Planet.new(:name => "Uranus", :form => "line", :type => "planet", :surface => "gaseous")

solar_system.add_item Planet.new(:name => "Jupiter", :form => "sphere", :type => "planet", :surface => "gaseous")

#next: in Ruby upper 1.9 if all geys are symbols, we can use such a syntax:
solar_system.add_item Planet.new(name: "Earth", form: "triangle", type: "planet", :surface => "liquid, hard")
solar_system.add_item Planet.new(name: "Pluto", form: "sphere", type: "dwarf planet", surface: "hard")
solar_system.add_item Planet.new(name: "Mars", form: "sphere", type: "planet", surface: "hard")

#next:
=begin
If two arguments defined in #initialize of SpaceObject, like this:

class SpaceObject
    ...
    def initialize(name, options = {})
        @type = options[:type]
        @name = name
        ...
    end
    ...
end

, then we can use such a syntax (without key ":name"):

solar_system.add_item Planet.new("Saturn", form: "sphere", type: "planet", surface: "gaseous")

BUT NOW we rewrite method initialize for parent class with two args, and also we have single param (options) in the method for child classes
To avoid an error we should put all arguments to super in child classes by ourselves;

class Planet < SpaceObject
    ...
    def initialize(options)
        @type = options[:type]
        super(options[:name], options)  ##!!!!!! name, hash
        ...
    end
    ...
end

=end
puts
puts "-----------------------------------------------------------"
solar_system.find_by_type "planet"
puts "---------"
solar_system.find_by_surface "hard"
puts "---------"

puts "Is solar_system an instance of SolarSystem? #{solar_system.kind_of? SolarSystem}"
puts "Is solar_system an instance of Planet? #{solar_system.kind_of? Planet}"
puts "---------"

planet = Planet.new({:name => "Earth", :form => "triangle", :type => "planet", :surface => "liquid, hard"})
star = Star.new({:name => "Sun", :form => "sphere", :type => "star", :temperature => "5778 K"})
space_object = SpaceObject.new({:name => "Sun", :form => "sphere", :type => "star"})
puts "Is plane an instance of Planet? #{planet.kind_of? Planet}"                        #true
puts "Is planet an instance of SpaceObject? #{planet.kind_of? SpaceObject}"             #true!!!! SpaceObject is a parent class
puts "---------"
puts "Is star an instance of Star? #{star.kind_of? Star}"
puts "Is star an instance of SpaceObject? #{star.kind_of? SpaceObject}"                 #true!!!! SpaceObject is a parent class
puts "---------"
puts "Is space_object an instance of SpaceObject? #{space_object.kind_of? SpaceObject}"
puts "Is solar_system an instance of Planet? #{space_object.kind_of? Planet}"
puts "---------"
puts

pluto = Planet.new({:name => "Pluto", :form => "sphere", :type => "dwarf planet", :surface => "hard"})
space_arr = [planet, star, space_object, pluto]
space_arr.each do |object|
    print "Object: #{object.class}, "
    print "Name: #{object.name}, "
    print "Type: #{object.type};"
    puts
end
puts "==========="

puts "For #{space_object.name}:"
print "#{space_object.class.ancestors}"
print "  -->  #{space_object.class}  -->  "
puts "#{space_object.class.descendants}"

puts "==========="

[:name, :kind_of?, :form, :type, :temperature].each do |method|
    space_arr.each do |obj|
        puts "#{obj.name} responds to method #name? #{obj.respond_to?(method)}"
        puts "++++++"
    end
end
puts "------------"
puts pluto.send(:name)
puts pluto.name
puts pluto.send(:show_name)
# puts pluto.show_name      #private
puts "!!!!!!!!!!!!!!!!!"
puts "Check if Application is singleton: #{Application.configure == Application.configure}"
puts
puts "##############################################"
puts

puts "Work with variables of instance and instance variables of class"
galaxy = Galaxy.new("Shiv Palpatine")
galaxy.rule
puts
Galaxy.rule "Dart Sidius"




