class Application
    #Instead the syntax of methods of class like that: def self.some_method...
    #or def Application.some_method...
    #we use this:

    class << self
        attr_accessor :name, :environment

        def configure #override
            unless @instance                 #nil?
                yield(self)
                puts "Loading files....."
                require_relative '../space/solar_system'
                require_relative '../space/space_objects/space_object'
                require_relative '../space/space_objects/planet'
                require_relative '../space/space_objects/star'

                require_relative '../space/space_objects/galaxy'
            end
            @instance ||= self          #INSTANCE variable of CLASS Application, self is for Application, not instance
                                        #And self is Application there !!!!
        end

        def admin(&closure)   #this method is like wrapper for singleton Admin
            @admin ||= Admin.new(&closure)
        end
    end

    class Admin
        class << self
            attr_accessor :name, :email, :login, :nation

            def new
                unless @instance
                    yield(self)
                end
                @instance ||= self  #self is Application::Admin !!!!!
            end

            def notify_on(day_of_week)
                @day_of_week = day_of_week
            end
        end
    end

end