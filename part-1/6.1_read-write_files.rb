def read_from_file(input_filename)
	teachers = []
	puts "Get info from file"
        puts "File does not exist" unless File.exists?(input_filename)

	File.open(input_filename, 'r') { |file|
		file.each_line do |line|
			prof_info_arr = line.to_s.split(" ")
			professor = [prof_info_arr[0], prof_info_arr[1]]
			teachers << professor
		end
	}
	teachers #return word is optional
end
#when the process of method is done, class File automatically close the file, so we need no extra deed like File.close 
#we should close file by self only when we set no block to method 'open' of class File

def show_items(items)
	items.each do |elem|
		puts elem
	end
end

def export_to_file(output_filename, teachers)
	count = 0
	teachers.each do |teacher|
		data = "teacher #{count+=1} is #{teacher[1]}, #{teacher[0]}"
	        File.open(output_filename, 'a') do |file|     # 'a' for append!! 'w' for rewriting file
			file.puts(data)		#!! input to file, not to terminal
			file << "-----------delimeter---------------"
			file << "\n"
		end
	end
end

input = ARGV[0].nil? ? '6.1_read-write_files-input.txt' : ARGV[0]
output = ARGV[1].nil? ? '6.1_read-write_files-output.txt' : ARGV[1]

teachers = read_from_file input
show_items teachers
export_to_file output, teachers
