def method_container(smth)
	smth = smth.to_s
	if smth.is_a? NilClass
		return "No argument for global method"
	elsif (smth.length > 1) && (smth.length < 5)
		return "Something is only 5 chars short"
	else
		smth.length.even? ? smth = HAHAH : smth.length.times { print "#{HAHAH}"}
		return "Done"
	end
end

def outer
	def inner
                puts "Hi!"
        end
end

HAHAH = "Hah-hah-hah"

puts method_container "35gcvgr5"
puts
puts method_container ""
puts 
puts method_container 1234
puts

puts outer.inner
