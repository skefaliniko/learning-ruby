class Book
    attr_accessor :title, :author, :price, :publisher
    
    def initialize(item)
        @title = item[:title]
        @author = item[:author]
        @price = item[:price]
    end
    
    def see_details
        yield(title)
        yield(author)
        yield(price)
    end
end

items_count = ARGV.length == 0 ? 4 : ARGV[0]

items_count.to_i.times do |iter|
	print "#{iter} "
end

puts
puts "Variable ARGV length #{ARGV.length}"
print "ARGV contains "
print ARGV
puts
puts "Start creating objects"
