#object identifier
class Alpha
	attr_accessor :name
	def initialize(name)
		@name = name
	end
end

class Omega < Alpha
	attr_reader :name
        def initialize(name)
                @name = name
        end
end

alpha = Alpha.new "Alpha"
omega = Omega.new "Omega"

puts "id for alpha: #{alpha.object_id}, id for omega: #{omega.object_id}"
puts

#------------------------------------------------------------------------

def smth(arg1, arg2)
	puts arg1.object_id
	puts arg2.object_id
	puts
	puts "...although they are equal" if arg1.to_s == arg2.to_s
	puts
end

smth "Alice", "Alice"
smth "Harry", "Potter"
smth "a", "а"

#---------------------------------------------------------------

def compare_obj_ids(one, two)
	puts "first id is #{one.object_id}"
	puts "second id is #{two.object_id}"
	if one.name.to_sym.object_id == two.name.to_sym.object_id
		puts "data but not objects are equal"
	else
		puts "something"
	end
end

#compare_obj_ids("12345", "12345")

alpha = Alpha.new "Alpha"
omega = Omega.new "Omega"
compare_obj_ids alpha, omega

alpha = Alpha.new "Alpha"
omega = Omega.new "Alpha"
compare_obj_ids alpha, omega

#------------------------------------------------------------------------

puts
puts "Input values separated with spaces"
arr_str = gets.chomp

arr = arr_str.split(" ")

hash_for_test = {}

arr.each do |item|
	hash_for_test[item.to_sym] = item.to_s
end

hash_for_test.each do |key, value|
	print "key: #{key}, value: #{value}"
	puts	
end
puts


second_hash = {:key => "initial val"}
second_hash[:lambda] = "lambda"
second_hash[:delta] = "delta"

hash_for_test.each do |key, value|
	second_hash[value.to_sym] = key.to_s
end

hash_for_test.each do |key, value|
        print "key: #{key}, value: #{value}"
        puts
end
puts

second_hash.each do |key, value|
        print "key: #{key}, value: #{value}"
        puts
end

#---------------------------------------------------------------------

class Book
	attr_accessor :title, :author, :genre

	def initialize(book)
		@title = book[:title]
		@author = book[:author]
		@genre = book[:genre]
	end
end


bookshelf = []

book1 = Book.new({ :title => "War and peace", :author => "Leo Tolstoy", :genre => "Romance"})
bookshelf << book1

book2 = Book.new({ :title => "Harry Potter and the Prisoner of Azkaban", :author => "Joahnn Rowling", :genre => "Fantasy"})
bookshelf << book2

bookshelf.each do |book|
	puts "#{book.author}. #{book.title}. #{book.genre}"
end

puts
#----------------------------------
# additional info from second part
#----------------------------------
puts "FROM PART II"
hogwarts = { :type => "school", :location => "Scotland", :director => "Albus Dumbledore"}
hogwarts.each_key do |key|
	puts "#{key.to_s.capitalize}: #{hogwarts[key]}"
end

