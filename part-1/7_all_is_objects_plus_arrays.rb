def print_info(variable)
	puts variable
	puts variable.class
	puts variable.methods
	puts "-----------------"
end

class Temp
end

temp = Temp.new

print_info("blah-blah-blah")
print_info(37675)
print_info(temp)
print_info(nil)
print_info(true)
puts

puts "harry potter".capitalize
puts "Size of 364764 is #{364764.size}"
puts 5465745.next

puts minerva = { :name => "Minerva", :sirname => "McGonagal" }
puts minerva.length

#----------------------------------

arr = Array.new(5)
puts arr

array = ["harry", "ron", "hermione", "albus", "severus", "minerva"]
puts
array.each_index { |item| print item }
puts
array.each { |item| item.capitalize! }
puts

array.pop
puts array
array.push "Mad Eye"
puts
puts array
array.shift
puts
puts array
